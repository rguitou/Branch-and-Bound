TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

QMAKE_CXXFLAGS += -std=c++11 -pedantic -Wall -Wextra
SOURCES += \
    DataSched.cpp \
    BBNode.cpp \
    BandB.cpp

include(deployment.pri)
qtcAddDeployment()

HEADERS += \
    DataSched.hpp \
    BBNode.hpp

