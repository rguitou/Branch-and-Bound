#include <iostream>
#include <limits>
#include <vector>
#include <queue>

#include "BBNode.hpp"
#include "DataSched.hpp"

using namespace std;

int main (int argc, char** argv){
  DataSched data;
  int bestUpperBound = numeric_limits<int>::max();
  vector<int> bestSolution;
  vector<BBNode> processStack;
  
  if (argc > 1){
    data.readDataFile(argv[1]);
  } else {
    char * filename = new char[256];
    cout << "Entrez le nom du fichier de données" << endl;
    cin >> filename;
    data.readDataFile(filename);
    delete [] filename;
  }

  BBNode* racine = new BBNode(&data);
  bestSolution = racine->combination;
  processStack.push_back(*racine);
  while(!processStack.empty()){
      cout<<"lol";
      BBNode currentNode = processStack.back();
      processStack.pop_back();

      if(currentNode.minBound < bestUpperBound)
      {

          for(int i = 0; i < currentNode.possibilities.size();i++)
          {
             BBNode* child = new BBNode(currentNode, currentNode.possibilities.at(i));
             currentNode.addChildren(*child);
          }

          for(BBNode son : currentNode.childrens)
          {

              if(son.minBound < bestUpperBound)
              {
                  if(son.isComplete())
                  {

                      bestUpperBound = son.minBound;
                      bestSolution = son.combination;
                  }
                  else
                  {

                      processStack.push_back(son);
                  }
              }
          }
      }
      //delete
  }

  
  cout << "Solution value : " << bestUpperBound << endl;
  cout << "Optimal solution : " << endl;
  cout << "<";
  for (vector<int>::iterator it = bestSolution.begin();
       it != bestSolution.end(); it++){
    cout << *it << ",";
  }
  cout << ">" << endl;
  return 0;
}
