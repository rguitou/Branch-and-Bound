#include "BBNode.hpp"

BBNode::BBNode(DataSched * ptr){
    dataPtr = ptr;
    DataSched d = *dataPtr;
    for(int i = 0; i < d.nbItems; i++){
        combination.push_back(-1);
        possibilities.push_back(i + 1);
    }
    minBound = 0;
    tmpTotTime = 0;
}

BBNode::BBNode(const BBNode & bbn, int inserted){
    dataPtr = bbn.dataPtr;
    bool isRoot = true;
    combination = bbn.combination;
    for (vector<int>::iterator it = combination.end()-1; it != combination.begin(); it--){
        if(*it != -1)
        {
            isRoot = false;
            it--;
            combination.erase(it);
            combination.insert(it, inserted);
            possibilities = bbn.possibilities;
            for(vector<int>::iterator it2 = possibilities.end()-1 ;it2 != possibilities.begin(); it2--)
            {
                if(inserted == *it2)
                {
                    possibilities.erase(it2);
                }
            }
            break;
        }
    }
    if(isRoot)
    {
        combination.erase(combination.end() - 1);
        combination.insert(combination.end(), inserted);
    }
    Evaluate();
}

void BBNode::Evaluate(){
    for(vector<int>::iterator it = combination.end() - 1; it != combination.begin(); it--)
    {
        DataSched datas = *dataPtr;
        int work = *it;
        if(work != -1)
        {
            tmpTotTime += datas.procTime[work - 1];
            int late = tmpTotTime - datas.dueDate[work-1];
            if(late > 0)
                minBound = late * datas.penalty[work - 1];

        }
    }

}

void BBNode::addChildren(const BBNode &child)
{
    childrens.push_back(child);
}

bool BBNode::isComplete()
{
    vector<int>::iterator it = combination.end() - 1;
    while(it != combination.begin())
    {
        if(*it == -1)
            return false;
        it--;
    }
    return true;
}

bool BBNode::operator> (const BBNode & node) const{

}



ostream & operator<< (ostream & os, const BBNode & bbn){


}
