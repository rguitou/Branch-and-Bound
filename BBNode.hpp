#ifndef BBNode_h
#define BBNode_h

#include <iostream>
#include <list>
#include <vector>

#include "DataSched.hpp"

using namespace std;

class BBNode{
public :
  DataSched * dataPtr;        /* Pointer on the instance data */

  list<BBNode> childrens;
  vector<int> combination;
  vector<int> possibilities;
  int minBound;
  int tmpTotTime;

  BBNode(DataSched * ptr);    /* constructor for the root node */
  BBNode(const BBNode & bbn, const int inserted); /* copy contructor, used to create children */
  void Evaluate();
  void addChildren(const BBNode & child);
  bool isComplete();
  bool operator> (const BBNode & node) const;

};

ostream & operator<< (ostream & of, const BBNode & bbn);

#endif
